import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      padding: const EdgeInsets.all(8),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 12, left: 100,),
                  child: Text('This is the Chief.', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24, color: Colors.amberAccent),
                  ),
                ),
                Text('"I need a weapon." - Master Chief', style: TextStyle(color: Colors.amberAccent, fontStyle: FontStyle.italic, fontWeight: FontWeight.w600, fontSize: 16),),
              ],
            ),
          ),
        ],
      ),
    );

    Color color = Theme.of(context).primaryColor;

    Widget buttonSection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildButtonColumn(Colors.amber, Icons.star, 'Highly Decorated\n        Officer'),
          _buildButtonColumn(Colors.amber, Icons.battery_charging_full, '          "Wake me, \n   when you need me."'),
          _buildButtonColumn(Colors.amber, Icons.attach_money , '      "Tell that\n to the Covenant."')
        ],
      ),
    );
    Widget textSection = Container(
        padding: const EdgeInsets.only(top: 24, left: 16, right: 16),
        child: Text(
          'Master Chief Petty Officer John-117 is a highly decorated and successful military officer from the planet Eridanus III.'
              ' Demonstrating a strong desire for victory and physical prowess from a young age, Dr. Catherine Halsey determined he\'d be an ideal candidate for the secret military initiative'
              ' known as the Spartan-II program. Quickly taking to his new role, John stood out amongst his peers as one of the strongest leaders and best Spartans alongside Jerome-92, '
              ' Kurt-051, and Fred-104. After graduating from the program, John, or Sierra-117, went on to become one of the greatest Spartans in history saving humanity as a whole on many occasions.',
          softWrap: true,
          style: TextStyle(color: Colors.amberAccent, fontSize: 14,),
        )
    );
    return MaterialApp(
      title: 'Layout Homework',
      home: Scaffold(
        backgroundColor: Color.fromARGB(100, 60, 90, 31),
        appBar: AppBar(
          title: Text('Master Chief', style: TextStyle(color: Colors.amberAccent),),
          backgroundColor: Color.fromARGB(100, 60, 140, 31),
        ),
        body: ListView(

          children: [
            Image.asset('images/chief.jpg', width: 600, height: 240, fit: BoxFit.cover,),
            titleSection,
            buttonSection,
            textSection
          ],
        ),
      ),
    );
  }

  Column _buildButtonColumn(Color color, IconData icon, String label){
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(icon, color: color,),
        Container(
          margin: const EdgeInsets.only(top: 2),
          child: Text(label, style: TextStyle(fontSize: 14, fontWeight: FontWeight.w600, color: color), softWrap: true,),
        ),
      ],
    );
  }
}
